# Cosmic-Piano
[ES] Piano Cosmic es un ejecutador de notas musicales limpias en clave de Sol, desarrollado en Angular 13.

[EN] Piano Cosmic is a musical note player in the key of G, developed in Angular 13.

[BR] O Piano Cosmic é um tocador de notas musicais na chave do G, desenvolvido em Angular 13.
